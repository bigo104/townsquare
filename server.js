var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var path = require('path');
var MainDBInterface = require("./data/mgoose/db-interface");

app.use(express.static('dist'));
app.use(bodyParser.json());

// set main app state from 1 st collections
app.get('/ads/data/:id', function (req, res) {
    return MainDBInterface.findById(req.params.id, function (err, ad) {
        if (!err) {
            return res.send(ad);
        } else {
            return console.log(err);
        }
    });
});


// insert docs based on form
app.post('/ads/insert-collection', function (req, res, next) {

    var submitNewData = new MainDBInterface({
        market: req.body.market,
        accountExecutive: req.body.accountExecutive,
        advertiser: req.body.advertiser,
        adPlacements: req.body.adPlacements,
        largerBuy: req.body.largerBuy,
        hiddenADDetails: req.body.hiddenADDetails,
        reviseDate: new Date
    });

    return submitNewData.save(function (err) {
        if (!err) {
            console.log("updated");
        } else {
            console.log(err);
        }
        return res.send("updated");
    });
});

// query all docs on revise date
app.use('/dashboard', function (req, res, next) {
   return MainDBInterface.find({"reviseDate":{"$gte": new Date(2016, 6, 20)}}).sort({"reviseDate":'desc'}).exec(function(err, ads){
       if(!ads.length){
           return res.send("No ads Created");
       }else{
           return res.send(ads);
       }
    })
});


app.listen(3000);