Test build for Townsquare Media
=====================================================

A Node implementation using express with mongo and react.

package.json contains all neccessary packages -- (npm packages are not included in this repo --"npm install"-- to download them)

**Config:**

Webpack(g) and babel for react and jsx transpiling...

Gulp(g) and Sass compillers for scss implementaion...
_______

Directory Structure
=====================================================
**SRC** directory contains all react and js implementation files 

**DATA** directory contains database and mongoose schema objects.

**DIST** is our distribution directory -- compiled css, bundled js and html files



**src/app-client.js** is the entry point for the client implementation.

**src/components/index.js** is the main app component that includes all other components as children

**src/components/ ---->  elements -- plugins -- and -- regions**   contain all react components and are broken up as such:

elements contain(inputs, radio button, forms, buttons ... etc),

plugins contain(radio group, datepicker), 

regions contain (header, body, footer);

_________



Code Chat
=====================================================

Web Server lives in server.js

All mongoose data schema's are held in data/mgoose directory

-- the mongo db is located in the data/db folder.

Either mongod --dbpath to this location or include db how you know best
_______


Installation
=====================================================
**npm install**

**sudo npm install webpack -g**

**sudo npm install gulp -g*

_______


Starting App
=====================================================
** open shell -- mongod --dbpath *full path to db* **  -- located in data/ -- use db directory

** open shell -- npm run watch ** (to run web pack and gulp sass watch)

** open shell -- shell npm run s ** (to get express server running on port 3000)

_______



Reference
=====================================================

React state is persisted in Mongo and read back in on subsequent loads.

To view hidden options when an ad placement is selected -- click on 'Billboard (formely pushdown)'

Date picker was implemented and is functional.

a revised date is automatically updated and persisted to mongo on api calls.


_______

Update
======
Dashboard Interface added to Application.

Documents are created with each form submissions -- persisted and automaticcaly read back into dashboard.

This build is purely for prototyping purposes -- real wold build requires refactor and research

_______

Contributors
=======================================================

100% built from the ground up by Omar A. Scott.