// gulp
var gulp = require('gulp');

// plugins

var clean = require('gulp-clean');
var concat = require('gulp-concat');
var runSequence = require('run-sequence');
var watch = require('gulp-watch');
var sass = require('gulp-ruby-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'expanded',
    sourceComments: true,
    sourceMapEmbed: true,
    sourceMapContents: true,
    data: ['./src/sass/**', './', 'src/sass'],
    includePaths: ['./src/sass/**', 'src/sass']

};


gulp.task('styles', function () {
    return sass('src/sass/**/*.scss', {
        //loadPath:'./app/sass/'
        sourcemap: true,
        style: 'expanded',
        lineNumbers: true,
        loadPath: __dirname + '/src/sass'
    })
        .on('error', sass.logError)
        .pipe(autoprefixer())
        .pipe(gulp.dest('./dist/css/'))
});

gulp.task('build', function () {
    runSequence(
        ['styles']
    )
});

gulp.task('watch', function () {
    gulp.watch(['src/**/*.html', 'src/**/*.js', 'src/**/*.scss'], {debounceDelay: 2000}, ['build'])

});

