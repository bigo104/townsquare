/**
 * Created by omarscott on 6/19/16.
 */


var db = require("./schema-class");

var configSchema = {
    market:String,
    accountExecutive:String,
    advertiser:String,
    adPlacements : [
        {selected:Boolean,name:String}
    ],
    largerBuy:false,
    hiddenADDetails:{
        station:String,
        flightStart:Date,
        flightEnd:Date
    },
    reviseDate:Date
};

var schema = new db.Schema(configSchema);


module.exports = db.model("ads", schema);




