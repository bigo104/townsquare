/**
 * Created by omarscott on 6/19/16.
 */


var mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/townsquare');
//console.log(mongoose.connection.readyState);

mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ');
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
    console.log('Mongoose default connection error: ' + err);
});

exports.Schema = function(schemaConfig){
    var hold = mongoose.Schema;
    return  new hold(schemaConfig);
};

exports.model = function(name, schema){
    return mongoose.model(name, schema);
};

