
var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'dist');
var APP_DIR = path.resolve(__dirname, 'src');

var config = {
  entry: APP_DIR + '/app-client.js',
  watch: true,
  output: {
    path: BUILD_DIR + '/js/',
    filename: 'bundle.js'
  },
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        include : APP_DIR,
        exclude: /(node_modules|server.js)/,
        loader : 'babel',
        query: {
                    presets: ['es2015','react'],
                    plugins:[
                    "add-module-exports",
                    "transform-es2015-modules-umd"
                    ]
                }
      }
      ]
    }

};

module.exports = config;