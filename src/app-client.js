/**
 * Created by omarscott on 7/3/16.
 */
import React from 'react';
import  App from './components/index';
import {render} from 'react-dom';
import $ from 'jquery';
import emitEvents from './events/events'

let allStateData = {};
let stubDbID = '/ads/data/' + '57839f17ceb2c23a0b8043d1';
let dashboardData = '/dashboard';


// get collection from mongo
$.get(stubDbID, function (data, textStatus, jqXHR) {
    allStateData.mainData = data;
    $.get(dashboardData,function(ddata){
        allStateData.dashData = ddata;
        renderApp();
    })
});

let renderApp = () => render( <App data = {allStateData} url={stubDbID}/>, document.getElementById('main'));




