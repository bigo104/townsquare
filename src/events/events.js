/**
 * Created by omarscott on 7/4/16.
 */
import $ from 'jquery';
const EventEmitter = require('events');
class MyEmitter extends EventEmitter {}
const myEmitter = new MyEmitter();

let modal = $('.modal');
let modalBacking = $('.backing-modal');
let modalClose = $('.close-message');



myEmitter.on('modalClose', () => {
    modal.hide();
    modalBacking.hide();
});

myEmitter.on('modalShow', () => {
    modal.show();
    modalBacking.show();
});


modalClose.on('click',()=>{
    myEmitter.emit('modalClose');
});

module.exports = myEmitter;