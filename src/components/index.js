/**
 * Created by omarscott on 7/3/16.
 */
// Main app entry point
import React from 'react';
import Header from './regions/header'
import Body from './regions/body'
import Footer from './regions/footer'
import _ from 'underscore'
import $ from 'jquery';
import emitEvents from '../events/events'
import Stringify from 'json-stringify-safe';
import Dashboard from './regions/dashboard';
import moment from 'moment';

class App extends React.Component {
    constructor(props) {
        super(props);

        // merge state from passed in mongo data

        this.state = {
            market: props.data.mainData.market,
            accountExecutive: props.data.mainData.accountExecutive,
            advertiser: props.data.mainData.advertiser,
            adPlacements: props.data.mainData.adPlacements,
            largerBuy: props.data.mainData.largerBuy,
            hiddenADDetails: {station: "", flightStart: moment(), flightEnd: moment()},
            hiddenPlack: "",
            dashboard: props.data.dashData,
            openDash: 'closed',
            selectedSort: 'reviseDate',
            date: moment()
        };

        // state handlers -- need to be refactored for DRY and better process flow
        this.updateMarket = (data)=> {
            if (this.state.market !== data.target.value) {
                this.setState({market: data.target.value});
            }
        };

        this.openDashboard = ()=> {
            this.setState({'openDash': 'open'});
        };

        this.closeDash = ()=> {
            this.setState({'openDash': 'closed'})
        };

        this.updateAcctExe = (data)=> {
            if (this.state.accountExecutive !== data.target.value) {
                this.setState({accountExecutive: data.target.value});
            }
        };

        this.updateAdvertiser = (data)=> {
            if (this.state.advertiser !== data.target.value) {
                this.setState({advertiser: data.target.value});
            }
        };

        this.updatePlacements = (data, hiddenPlackName)=> {
            let tempData = _.filter(this.state.adPlacements, (item)=> {
                if (item.name === data.name) {
                    item.selected = data.selected;
                }
                return item;
            });
            this.setState({adPlacements: tempData});
            this.setState({
                hiddenPlack: hiddenPlackName
            })
        };

        this.updateLargerBuy = (data)=> {
            this.setState({
                largerBuy: data
            })
        };

        this.updateStations = (data)=> {
            if (this.state.hiddenADDetails.station !== data.target.value) {
                let tempData = this.state.hiddenADDetails;
                tempData.station = data.target.value;
                this.setState({hiddenADDetails: tempData});
            }
        };

        this.updateStartDate = (data) => {
            let tempData = this.state.hiddenADDetails;
            tempData.flightStart = moment(data._d);
            //console.log(tempData.flightStart);
            this.setState({hiddenADDetails: tempData});
        };

        this.updateEndDate = (data) => {
            let tempData = this.state.hiddenADDetails;
            tempData.flightEnd = moment(data._d);
            this.setState({hiddenADDetails: tempData});
        };

        this.updateCollection = () => {
            let self = this;
            let sendData = Stringify(self.state, null);
            $.ajax({
                url: '/ads/insert-collection',
                type: "POST",
                data: sendData,
                contentType: "application/json",
                dataType: 'text',
                success: function (data, textStatus, jqXHR) {
                    //console.log("Post resposne:");
                    //console.dir(data);
                    //console.log(textStatus);
                    //console.dir(jqXHR);
                    emitEvents.emit('modalShow');
                    self.getLatestPost();
                },
                error: function (err) {
                    //console.log("error : ", err);
                }
            });
        };

        this.getLatestPost = ()=> {
            $.get('/dashboard', (data) => {
                this.setState({'dashboard': data})
            })
        };

        this.sortDashboard = (data)=> {
            this.setState({'dashboard': data.data, 'selectedSort': data.selectedSort});
        };


        // loosing context when passed down the chain to node children on event listeners --
        // force bind to retain context
        this.updateMarket = this.updateMarket.bind(this);
        this.updateAcctExe = this.updateAcctExe.bind(this);
        this.updateAdvertiser = this.updateAdvertiser.bind(this);
        this.updatePlacements = this.updatePlacements.bind(this);
        this.updateLargerBuy = this.updateLargerBuy.bind(this);
        this.updateStations = this.updateStations.bind(this);
        this.updateCollection = this.updateCollection.bind(this);
        this.updateStartDate = this.updateStartDate.bind(this);
        this.updateEndDate = this.updateEndDate.bind(this);
        this.openDashboard = this.openDashboard.bind(this);
        this.closeDash = this.closeDash.bind(this);
        this.getLatestPost = this.getLatestPost.bind(this);
        this.sortDashboard = this.sortDashboard.bind(this);
    }

    componentWillMount() {

    }

    componentDidUpdate() {
        //console.log("did update State:  ", this.state);
    }

    render() {
        return (
            <div className="wrapper" id="wrapper">

                <Dashboard show={(typeof(this.state.dashboard) === 'object')}{...this.state}
                           closeDash={this.closeDash}
                           sortData={this.sortDashboard}
                    />

                <Header heading={"CAMPAIGN DETAILS"}/>
                <Body {...this.state}
                    updateMarket={this.updateMarket}
                    updateAcctExe={this.updateAcctExe}
                    updateAdvertiser={this.updateAdvertiser}
                    updatePlacements={this.updatePlacements}
                    updateLargerBuy={this.updateLargerBuy}
                    updateStations={this.updateStations}
                    updateCollection={this.updateCollection}
                    updateStartDate={this.updateStartDate}
                    updateEndDate={this.updateEndDate}
                    openDashboard={this.openDashboard}
                    />
                <Footer />
            </div>
        )
    }
}


module.exports = App