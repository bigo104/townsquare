/**
 * Created by omarscott on 7/3/16.
 */

import React from 'react';
//var React = require('react');

let Radio = React.createClass({
    handleChange(event){
        //let tempChecked = !this.props.checked;
        //this.props.checked = !this.props.checked;
        this.props.change(event);
    },

    getDefaultProps(){
        return{
            checked:false
        }
    },

    render(){
        return (
            <input type="radio" value={this.props.value} onChange={this.handleChange} name={this.props.name}
                   checked={this.props.checked}/>
        )
    }
});

module.exports = Radio;
