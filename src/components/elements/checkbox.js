/**
 * Created by omarscott on 7/3/16.
 */
import React from 'react';
//var React = require('react');

let Checkbox = React.createClass({
    handleChange(){
        let tempChecked = !this.props.checked;
        let sendLabel = (tempChecked === true)?this.props.label:"";
        this.props.change({selected:tempChecked,name:this.props.label},sendLabel);
    },

    getDefaultProps(){
        return{
            checked:false
        }
    },
    componentDidMount(){
        if(this.props.label === 'Billboard (formely pushdown)' && this.props.checked ){
            console.info("this is working");
            this.props.change({selected:true,name:this.props.label},this.props.label);
        }
    },

    render(){
        return(
            <div className = "checkbox-wrap">
                <input type="checkbox" checked = {this.props.checked} onChange ={this.handleChange} name={this.props.label}/>
                <label>{this.props.label}</label>
            </div>
        )
    }
});


module.exports = Checkbox;