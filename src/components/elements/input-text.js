/**
 * Created by omarscott on 7/3/16.
 */
import React from 'react';
//var React = require('react');

let Input = React.createClass({

    handleChange(event){

        this.props.change(event);
    },

    render(){
        return (
            <div className="input-wrap ">
                <label className="tester-home">{this.props.label}</label>
                <input type="text" placeholder={this.props.placeholder} value={this.props.value}
                       onChange={this.handleChange} className="form-control "/>
            </div>
        )
    }
});


module.exports = Input;