/**
 * Created by omarscott on 7/3/16.
 */
import React from 'react';

let Button = React.createClass({

    handleChange(event){
        this.props.updateCollection();
    },

    render(){
        return (
            <button  onClick={this.handleChange} className="btn btn-default">
                SUBMIT
            </button>
        )
    }
});


module.exports = Button;