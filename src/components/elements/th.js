/**
 * Created by omarscott on 7/21/16.
 */

import React from 'react';
//var React = require('react');

let Th = React.createClass({

    sortView(){
        if(this.props.sort){
            this.props.sort(this.props.cname);
        }
    },

    render(){
        return (
            <th onClick = {this.sortView} className={(this.props.selected === this.props.cname)?'selected':'deselected'} >
                {this.props.name}
            </th>
        )
    }
});


module.exports = Th;