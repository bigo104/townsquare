/**
 * Created by omarscott on 7/21/16.
 */

import React from 'react';
import Th from '../elements/th';
import _ from 'underscore';
let month_names_short = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

class Dashboard extends React.Component {

    constructor(props) {
        super(props);
    }

    closeDashBoard(){
       this.props.closeDash();
    }
    // format dates
    dateFormater(data){
        let formatDate = new Date(data);
        return month_names_short[formatDate.getMonth()] + ",  " + formatDate.getDate() + ",  " +  formatDate.getFullYear();
    }
    // print asd placements
    getAdPlacements(data){
        return data.map((item,i)=>{
            return <p key={i}>{item.selected ? item.name : ""}</p>
        });
    }

    // sorting functions for grid layout
    setAccountSort(sortThis){
        let sort = _.sortBy(this.props.dashboard,(item)=>
            item[sortThis]
        );
        let data ={
            data:sort,
            selectedSort:sortThis
        };
        this.props.sortData(data);
    }

    setAccountDateSort(sortThis){
        let sort = _.sortBy(this.props.dashboard,(item)=>
                new Date(item[sortThis])
        );
        let data ={
            data:sort.reverse(),
            selectedSort:sortThis
        };
        this.props.sortData(data);
    }

    setAccountStationSort(sortThis){
        let sort = _.sortBy(this.props.dashboard,(item)=>
                item.hiddenADDetails[sortThis]
        );
        let data ={
            data:sort,
            selectedSort:sortThis
        };
        this.props.sortData(data);
    }

    setFlightDateSort(sortThis){

        let sort = _.sortBy(this.props.dashboard,(item)=> {
                new Date(item.hiddenADDetails[sortThis])
            }
        );
        let data ={
            data:sort,
            selectedSort:sortThis
        };
        this.props.sortData(data);
    }

    // offload render based on conditional if data exist
    renderData(){
        if(this.props.show) {
            return this.props.dashboard.map((data, i)=>
                    <tr key={i}>
                        <td>{this.dateFormater(data.reviseDate)}</td>
                        <td>{data.market}</td>
                        <td>{data.accountExecutive}</td>
                        <td>{data.advertiser}</td>
                        <td>{data.largerBuy}</td>
                        <td>{this.getAdPlacements(data.adPlacements)}</td>
                        <td>{data.hiddenADDetails.station}</td>
                        <td>{this.dateFormater(data.hiddenADDetails.flightStart)}</td>
                        <td>{this.dateFormater(data.hiddenADDetails.flightEnd)}</td>
                    </tr>
            )
        }
    }

    render(){
        return(
            <div className={`dashboard ${this.props.openDash}`}>
                <h2> Ad Dashboard <div className="close-dash" onClick = {this.closeDashBoard.bind(this)}>X - Close Dashboard</div> </h2>
                <table>
                    <thead>
                    <tr>
                        <Th sort = {this.setAccountDateSort.bind(this)} selected={this.props.selectedSort} cname = "reviseDate" name="Date"/>
                        <Th sort = {this.setAccountSort.bind(this)} selected={this.props.selectedSort} cname = "market" name="Market"/>
                        <Th sort = {this.setAccountSort.bind(this)} selected={this.props.selectedSort} cname = "accountExecutive" name="Account Executive"/>
                        <Th sort = {this.setAccountSort.bind(this)} selected={this.props.selectedSort} cname = "advertiser" name="Advertiser"/>
                        <Th sort = {this.setAccountSort.bind(this)} selected={this.props.selectedSort} cname = "largerBuy" name="Larger Buy"/>
                        <Th selected={this.props.selectedSort} cname = 'adPlacements' name="Ad Placements"/>
                        <Th sort = {this.setAccountStationSort.bind(this)} selected={this.props.selectedSort} cname = "station" name="Station"/>
                        <Th sort = {this.setFlightDateSort.bind(this)} selected={this.props.selectedSort} cname = 'flightStart' name="Flight Start"/>
                        <Th sort = {this.setFlightDateSort.bind(this)} selected={this.props.selectedSort} cname = 'flightEnd' name="Flight End"/>
                    </tr>
                    </thead>
                    <tbody>
                    {this.renderData()}
                    </tbody>
                </table>
            </div>
        )
    }

}

module.exports = Dashboard;
