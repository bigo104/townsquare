/**
 * Created by omarscott on 7/3/16.
 */
import React from 'react';
import Input from '../elements/input-text'
import Checkbox from '../elements/checkbox'
import Radio from '../elements/radio'
//import DatePickerPlug from '../plugins/date-picker'
import DatePicker from 'react-datepicker'
import RadioGroup from '../plugins/radio-group'
import Button from '../elements/submit-button'
import Display from './display'


let Body = React.createClass({

    getDefaultProps(){
        return {
            market: "",
            accountExecutive: "",
            advertiser: ""
        }
    },

    openDash(){
        this.props.openDashboard();
    },

    handleStartDateChange(date){
        //console.log(date._d);
        this.props.updateStartDate(date);
    },

    handleEndDateChange(date){
        //console.log(date._d);
        this.props.updateEndDate(date);
    },

    // here we center all body rendering into this one component for neatness
    render(){
        return (
            <div id="body-wrapper ">


                <Input placeholder="Enter a Market Name" value={this.props.market} label="Market"
                       change={this.props.updateMarket}/>

                <Input placeholder="Enter a name" value={this.props.accountExecutive} label="Account Executive"
                       change={this.props.updateAcctExe}/>

                <Input placeholder="Enter an advertiser" value={this.props.advertiser} label="Advertiser"
                       change={this.props.updateAdvertiser}/>


                <div className="ad-placements">

                    <p>Ad Placements</p>

                    {this.props.adPlacements.map((item, i)=>
                            (<Checkbox key={i} label={item.name} checked={item.selected}
                                       change={this.props.updatePlacements}/>)
                    )}
                    <p className = "mice-print"> if you selected Audience Extension, sizes 300x250, 728x90, 160,600 and 320x50 will automatically be created</p>

                </div>


                <div className="radio-wrap">
                    <p>Is this order a part of a larger buy from this client? *</p>
                    <RadioGroup active={this.props.largerBuy} change={this.props.updateLargerBuy}/>
                    <p className = "mice-print"> Please select yes if this digital order is part of a larger buy(other digital products, radio, live events, etc.)</p>

                </div>


                <Display if={this.props.hiddenPlack === 'Billboard (formely pushdown)'}>
                    <h3>Billboard Traffic Details</h3>
                    <Input placeholder="Enter Stations" value={this.props.hiddenADDetails.station} label="Stations(s)*"
                           change={this.props.updateStations}/>

                    <p>First Flight Start Date *</p>

                    <DatePicker selected={this.props.hiddenADDetails.flightStart} onChange={this.handleStartDateChange}/>

                    <p>First Flight End Date *</p>

                    <DatePicker selected={this.props.hiddenADDetails.flightEnd} onChange={this.handleEndDateChange}/>


                </Display>

                <Button updateCollection={this.props.updateCollection}/>
                <button className = "dashboard-open btn btn-default" onClick={this.openDash}> DASHBOARD </button>

            </div>
        )
    }
});


module.exports = Body;