/**
 * Created by omarscott on 7/4/16.
 */

import  React from 'react';

let Display = React.createClass({
    render() {
        return (this.props.if) ?  <div className="hidden-details">{this.props.children}</div> : null;
    }
});

module.exports = Display;