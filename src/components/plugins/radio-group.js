/**
 * Created by omarscott on 7/4/16.
 */
import React from 'react';
import {RadioGroup, Radio} from 'react-radio-group';

let rGroup = React.createClass({
    handleChange(data){
        //console.log("this.props.active", event);
        this.props.change(data);
    },
    render(){
        return (
            <RadioGroup name="largerbuy" selectedValue={this.props.active} onChange={this.handleChange}>
                <div className="radio-item">
                    <Radio value="true"/>Yes
                </div>
                <div className="radio-item">
                    <Radio value="false"/>No
                </div>
            </RadioGroup>

        )
    }

});

module.exports = rGroup;