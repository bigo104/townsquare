/**
 * Created by omarscott on 7/3/16.
 */
import React from 'react'
import DatePicker from 'react-datepicker'

let DatePickers = React.createClass({
    handleChange(date){
        this.props.updateDate(date);
    },
    render(){
        return (
            <DatePicker selected={this.props.date} onChange={this.handleChange} />
        )
    }
});


module.exports = DatePickers;